<!DOCTYPE html>

<!--
    Document: Proyecto fin de carrera UAB-2010/2011
    Author: Marta Laperal Martin    
-->

<section>
    <form method="POST" action="acceso/login.php" type="multipart/form-data">                        
        
        <label for='username' id='user'>Usuario:</label>
        <input type='text' name='username' id='username' size='10' required='required'/>                            
    
        <label for='userpass' id='cont'>Contrase&ntilde;a:</label>
        <input type='password' name='userpass' id='userpass' size='10' required='required'/>
    
        <div class='botones'> 
          <input type='submit' name='submit' id='submit' value='Entrar'/> 
          <input type='reset' name='reset' id='reset' value='Reiniciar'/> 
        </div>                      
    </form> 
</section>

<?php
session_start();
$_SESSION['logeado']=0 ;
?>

