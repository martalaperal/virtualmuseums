<!DOCTYPE html>

<!--
    Document: Proyecto fin de carrera UAB-2010/2011
    Author: Marta Laperal Martin    
-->


<article class="posiciona">
    <figure id="autora"></figure>
    <p class="titulo">Acerca de mi...</p>
    <div id="parrafoizq">
        <p>Mi nombre es Marta Laperal Mart&iacute;n y nac&iacute; en Granada en el a&ntilde;o 1986.</p>
        <p>Estudi&eacute; la carrera de <i>"Inform&aacute;tica T&eacute;cnica de Sistemas"</i> junto a la de <i>"Ingenier&iacute;a Inform&aacute;tica"</i>.</p>
        <p id="nuevo">Como proyecto de fin de carrera eleg&iacute; el siguiente:</p>
        <p id="proyecto">"Generaci&oacute;n de visitas virtuales a museos 3D para su inclusi&oacute;n en p&aacute;ginas web"</p>                    
    </div>
    
    <p>Consiste en la creaci&oacute;n de una p&aacute;gina web a la que podr&aacute; acceder todo tipo de persona, m&aacute;s
        estar&aacute; dedicada al sector del Arte. En &eacute;sta los usuarios podr&aacute;n registrarse y acceder posteriormente a una zona
        privada donde dise&ntilde;ar&aacute;n un museo a partir de una c&oacute;moda paleta de dibujo, pudiendo elegir la distribuci&oacute;n que quieran para su futura exposici&oacute;n, colocando los objetos varios que definen la galeria de arte donde m&aacute;s les precie.
    </p>                
    
    <div id="izq">
        <figure id="autora2"></figure>
        <p id="frase">~~ La libertad de so&ntilde;ar ~~</p>
    </div>
    <div id="parrafoder">
        <p>Una vez dise&ntilde;ado el museo, los usuarios podr&aacute;n visitar su galer&iacute;a en 3 dimensiones, por la que se les permitir&aacute; realizar un recorrido a trav&eacute;s como si estuviesen en el interior de &eacute;sta.</p>        
    </div>
    
    <div id="debajo">
        <p id="herramientas">Para realizar este proyecto me he servido de los lenguajes de programaci&oacute;n web:
        <ul id="tools"><li>Html 5, Css 3, JavaScript, Jquery, Php y MySql.</li></ul></p> 
        <p>Respecto al dise&ntilde;o, he creado los fondos, dibujos y efectos varios mediante el programa "Gimp 2". Se ha elegido la
        distribuci&oacute;n de colores malva, lila, beige, marr&oacute;n, verde oscuro... para conseguir una interfaz bonita y suave a la vista.</p>
        <p id="comentarios">Por mi parte, este proyecto ha sido un reto personal muy interesante y productivo, que ha supuesto un largo trabajo de investigaci&oacute;n
        y desarrollo. No obstante ha sido divertido y estoy muy contenta de haber tenido la oportunidad de llevarlo a cabo.
        Para m&aacute;s informaci&oacute;n, p&oacute;ngase en contacto conmigo en la siguiente direcci&oacuten:</p>
        <figure id="e_mail"></figure>
    </div>
</article>


<?php $_SESSION['pagina'] = 2 ; ?>
