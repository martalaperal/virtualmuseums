<!DOCTYPE html>

<!--
    Document: Proyecto fin de carrera UAB-2010/2011
    Author: Marta Laperal Martin    
-->
    
<?php

    if ( !isset($_SESSION['logeado']) )
        session_start() ;

    if (isset($_SESSION['logeado'])){

        $logeado=$_SESSION['logeado'];        
        $nombre=$_SESSION['nombre'];

        if ( $logeado==2 ){ ?>

            <article id="paleta">
                
                <section id="encabezado">
                    <p>Dise&ntilde;a tu museo</p>
                </section>
                
                <section id="rejilla">
                    <aside>
                        <button id="puerta" class="boton2" title="Insertar puerta" onclick="javascript:CargarObjeto('puerta')"></button>
                        <button id="ventana" class="boton2" title="Insertar ventana" onclick="javascript:CargarObjeto('ventana')"></button>
                        <button id="pared" class="boton2" title="Insertar pared" onclick="javascript:CargarObjeto('pared')"></button>
                        <button id="obra" class="boton2"  title="Insertar obra" onclick="javascript:CargarObjeto('obra')"></button>
                        <button id="suelo" class="boton2" title="Borrar" onclick="javascript:CargarObjeto('suelo')"></button>
                    </aside>
    
                    <canvas id="micanvas" width="680" height="630">
                        <script type="text/javascript">
                            MuseoAnterior();                            
                        </script>
                    </canvas>
                    
                    <?php
                        $_SESSION['logeado']=$logeado;        
                        $_SESSION['nombre']=$nombre;
                    ?>
                    <button id="fotos" class="boton3" title="Subir las obras" onclick="javascript:CargarImagenes()">Cargar Im&aacute;genes</button>
                    <button id="guardar" class="boton3" title="Museo terminado" onclick="javascript:Cancela()">Guardar Dise&ntilde;o</button>
                    <div><button id="museo3d" class="boton3" title="Museo 3D">Museo Virtual final</button></div>
                </section>
                
                <section id="panel" name="panel"></section>                

            </article>
            
            <script type="text/javascript">
            
                $(document).ready(
                    
                    function(){
                        $("#fotos").click(
                            function(evento){
                                evento.preventDefault();                                Panel();
                            });
                    })             
            
            
                $(document).ready(
                    
                    function(){
                        $("#museo3d").click(
                            function(evento){
                                evento.preventDefault();                                Revisa(); 
                            });
                    })
            </script>

        <?php    
        }
        else{
            include("../index/inicio.php") ;
        ?>
            <script type="text/javascript">
                alert("Zona reservada para usuarios registrados");
            </script>
    <?php
        }
    }
    else{
        include("../index/inicio.php") ;
    ?>
        <script type="text/javascript">
                alert("Zona reservada para usuarios registrados");
        </script>
    <?php
    }
    
    include("subida.php") ;
    
    $_SESSION['pagina'] = 3 ; 
?>
    

    
    
