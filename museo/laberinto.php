<!DOCTYPE html>


<script type="text/javascript" src="js/glMatrix-0.9.5.min.js"></script>
<script type="text/javascript" src="js/webgl-utils.js"></script>
<script type="text/javascript"  src="js/laberinto.js"></script> 

<script id="shader-fs" type="x-shader/x-fragment">

    #ifdef GL_ES
    precision highp float;
    #endif

    varying vec2 vTextureCoord;

    uniform sampler2D uSampler;

    void main(void) {
        gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));
    }
</script>

<script id="shader-vs" type="x-shader/x-vertex">

    attribute vec3 aVertexPosition;
    attribute vec2 aTextureCoord;

    uniform mat4 uMVMatrix;
    uniform mat4 uPMatrix;

    varying vec2 vTextureCoord;

    void main(void) {
        gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
        vTextureCoord = aTextureCoord;
    }
</script>


<section id="encabezado">
    <p>Recorrido en 3 dimensiones</p>
</section>

<canvas id="dibujo" width="744" height="500">
    
    <script type="text/javascript">
        webGLStart();
    </script>

    <article id="cuentaatras"> </article>
    
</canvas>


<div id="cargando">Loading world...</div>

<section id="guia">
   <p id="colum">
    Use las teclas <code>W</code> / <code>A</code> / <code>S</code> / <code>D</code> para moverse hacia delante, la izquierda, atr&aacute;s y la derecha respectivamente.
  </p>
  <button id="listado" class="boton3" title="Autores y sus obras">Acerca de las obras</button>
  
  <script type="text/javascript">
  
    var unavez = false ;
    
    $(document).ready(
              function(){
                  $("#listado").click(
                      function(evento){
                          evento.preventDefault();
  
                        HazListado();                      
                        
                        if ( unavez == true )
                            $('#lista').toggle();
                            
                        if ( unavez == false )
                            unavez = true ;
                      });
              })
  </script>
  
</section>

<section id="lista" name="lista"></section>   

