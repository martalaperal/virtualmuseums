<?php
    if (!isset($_SESSION['aviso']))
        $_SESSION['aviso']=0; // se inicializa
    
    $aviso=$_SESSION['aviso'];

    
    if ($aviso==1){              
        echo "<script language='javascript'>";
        echo "alert('Error. La imagen ha de ser de formato .jpeg.')";
        echo "</script>";
    }
    else if($aviso==2){
        echo "<script language='javascript'>";
        echo "alert('Error. El tama\u00f1o m\u00e1ximo permitido es 1024x1024.')";
        echo "</script>";
    }
    else if($aviso==3){
        echo "<script language='javascript'>";
        echo "alert('Error al subir la foto. Int\u00e9ntelo de nuevo.')";
        echo "</script>";
    }
    else if($aviso==4){
        echo "<script language='javascript'>";
        echo "alert('Carga de fotos realizada.')";
        echo "</script>";
    }
    
    $_SESSION['aviso']=0;
 
?>