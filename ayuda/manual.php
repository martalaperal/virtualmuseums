<!DOCTYPE html>

<!--
    Document: Proyecto fin de carrera UAB-2010/2011
    Author: Marta Laperal Martin    
-->


  <article class="manual">
        <p class="titulo">Manual de usuario</p>
        <section id="bloque">
            <p>&iexcl;Bienvenido al dise&ntilde;ador de museos virtuales online!</p>            
            <p>En las distintas secciones encontrar&aacute; lo siguiente:</p>
            <ul id="pestania2">
                <li>
                    <b>Inicio:</b> Para volver a la la p&aacute;gina principal (Home).
                </li>
                <li>
                    <b>Nuevo usuario:</b> Para registrarse en la p&aacute;gina web y poder acceder a la parte privada de &eacute;sta.
                </li>
                <li>
                    <b>Acerca del autor:</b> Para conocer a la creadora de este proyecto, informaci&oacute;n de contacto y ver una descripci&oacute;n detalla del trabajo.
                </li>
                <li>
                    <b>Museo Virtual:</b> Para acceder a la paleta de dibujo donde podr&aacute; crear su museo particular y posteriormente poder visualizarlo en 3 dimensiones. Es una zona reservada para usuarios registrados.
                </li>
                <li>
                    <b>Ayuda:</b> Para consultar c&oacute;mo navegar por la p&aacute;gina web, secci&oacute;n en la que usted se encuentra actualmente.
                </li>
            </ul>
            <p>Para empezar, si a&uacute;n no es usuario, podr&aacute; registrarse haciendo clic en la pesta&ntilde;a <b>Nuevo usuario</b>. A continuaci&oacute;n deber&aacute; rellenar los campos obligatorios y los opcionales si lo desea, pulsando el bot&oacute;n <b>Aceptar</b> para terminar o bien el bot&oacute;n <b>Reiniciar</b> para empezar de nuevo.</p>
            <p>Una vez registrado, podr&aacute; identificarse introduciendo su nombre de usuario y contrase&ntilde;a en el formulario de arriba a la derecha, con el bot&oacute;n <b>Entrar</b> para proceder a la autenticaci&oacute;n o el bot&oacute;n <b>Reiniciar</b> para escribir de nuevo.</p>
            <p>Ser&aacute; entonces el momento de pulsar la pesta&ntilde;a <b>Museo Virtual</b>, donde encontrar&aacute; todo lo necesario para realizar su dise&ntilde;o. En esta secci&oacute;n ver&aacute; a su derecha una cuadrilla representativa del museo en un plano de dos dimensiones, donde cada casilla representa un objeto; mientras que a su izquierda ver&aacute; una barra de herramientas, con la cual se podr&aacute;n insertar los siguientes objetos:</p>
            <ul id="pestania1">
                <li>
                    <figure id="puerta" class="mini"></figure>
                    <section id="descripcion">
                        <b>Una puerta</b>
                        <label>, bajo las siguientes restricciones:</label>
                        <ul id="pestania2">
                            <li>Colocar la puerta donde se halle una pared.</li>
                            <li>No colocar sobre otro objeto que no sea pared o adyacente a un cuadro.</li>
                        </ul>
                    </section>
                </li>
                <li>
                    <figure id="ventana" class="mini"></figure>
                    <section id="descripcion">
                        <b>Una ventana</b>
                        <label>, bajo las siguientes restricciones:</label>
                        <ul id="pestania2">
                            <li>Colocar la ventana donde se halle una pared.</li>
                            <li>No colocar sobre otro objeto que no sea pared o adyacente a un cuadro.</li>
                        </ul>
                    </section>
                </li>
                <li>
                    <figure id="pared" class="mini"></figure>
                    <section id="descripcion">
                        <b>Una pared</b>
                        <label>, bajo las siguientes restricciones:</label>
                        <ul id="pestania2">
                            <li>Solamente se puede colocar sobre el objeto suelo.</li>
                        </ul>
                    </section>
                </li>
                <li>
                    <figure id="obra" class="mini"></figure>
                    <section id="descripcion">
                        <b>Un cuadro</b>
                        <label>, bajo las siguientes restricciones:</label>
                        <ul id="pestania2">
                            <li>Colocar el cuadro adyacente a una pared, sobre la que se colocar&aacute;.</li>
                            <li>No colocar sobre otro objeto.</li>
                        </ul>
                    </section>
                </li>
                <li>
                    <figure id="suelo" class="mini"></figure>
                    <section id="descripcion">
                        <b>Una baldosa</b>
                        <ul id="pestania2">
                            <li>Este objeto representa al suelo, m&aacute;s su funci&oacute;n en la paleta es la de borrar, para ello pulse sobre un objeto y &eacute;ste se eliminar&aacute; colocando una baldosa en su lugar.</li>
                        </ul>
                    </section>
                </li>                
            </ul>
            <p>Para insertar estos objetos, deberemos pulsar su bot&oacute;n correspondiente y a continuaci&oacute;n pinchar en los cuadros de la rejilla donde queramos insertarlo, entonces nos aparecer&aacute; sobre el cuadro lo siguiente</p>
            <ul id="pestania1">
                <li>
                    <figure id="azul" class="mini"></figure>
                    <section id="descripcion2">
                        <p>Si el cuadro aparece en azul, significa que podemos insertar el objeto elegido en esa casilla.</p>
                    </section>
                </li>
                <li>
                    <figure id="rojo" class="mini"></figure>
                    <section id="descripcion2">
                        <p>Si el cuadro aparece en rojo con una cruz, significa que estamos infringiendo alguna de las reglas anteriormente
mencionadas (respecto a ese objeto en concreto).</p>
                    </section>
                </li>
            </ul>
            <p>Una vez realizado el dise&ntilde;o a nuestro gusto, deber&aacute proceder a subir las im&aacute;genes correspondientes a los cuadros insertados, para ello haga clic en el bot&oacute;n <b>Subir im&aacute;genes</b>, carge las fotos de su directorio y pulse <b>Aceptar</b> para terminar o bien <b>Cancelar</b> para salir sin cargar las im&aacute;genes. Las fotos cargadas deber&aacute;n ser cuadradas (ancho y alto potencias de dos) y el n&uacute;mero m&aacute;ximo a subir es 20. Si borra alg&uacute;n cuadro, la numeraci&oacute;n se actualizar&aacute; y si desea a&ntilde;adir o modificar una imagen, s&oacute;lo tendr&aacute; que rellenar la imagen en cuesti&oacute;n, dejando el resto de cuadros en blanco.</p>
            <p>Para finalizar con la parte del dise&ntilde;o, podr&aacute; guardarlo pulsando el bot&oacute;n situado a la derecha <b>Guardar Museo</b>.</p>    
            <p id="parte3d">Pasamos entonces a la parte virtual, para ello haga clic en <b>Museo Virtual Final</b> y la p&aacute;gina web le llevar&aacute; automaticamente a una nueva secci&oacute;n con la visualizaci&oacute;n de su museo en 3 dimensiones.</p>
            <p>Inicialmente el visor del museo virtual se encontrar&aacute; en la parte central inferior de la antigua paleta en 2 dimensiones. Para moverse a trav&eacute;s de la animaci&oacute;n deber&aacute; usar las siguientes teclas:</p>
            <ul>
                <li id="tabla">
                    <ul id="pestania3">
                        <li><b>W</b>: Andar hacia delante.</li>
                        <li><b>A</b>: Girar a la izquierda.</li>
                        <li><b>S</b>: Retroceder hacia atr&aacute;s.</li>
                        <li><b>D</b>: Girar a la derecha.</li>
                    </ul>
                </li>
                <li id="tabla"><figure id="teclas" class="mini"></figure></li>
            </ul>
            <p>De este modo, podr&aacute; realizar un recorrido por el interior y exterior de su museo virtualmente, andando a trav&eacute;s de los pasillos, cruzando las puertas y contemplando cada una de sus obras con detalle.</p>
        </section>
  </article>
  
  <?php $_SESSION['pagina'] = 4 ; ?>