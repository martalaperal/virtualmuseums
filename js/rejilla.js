
/* Rejilla de 29x27 cuadrados de 23 p�xeles
 * Tipos de objeto:
 *  0 - Suelo.
 *  1 - Pared.
 *  2 - Puerta.
 *  3 - Ventana.
 *  4 - Obra.
*/

var listaobj = new Array(29);
var idObj ;
var ncuadros = 0 ;
var fin = false ;
var listaObr = new Array() ;
var nimag = 0 ;
      
for (c=0; c<29; ++c)
    listaobj[c] = new Array(27);

for (i=0; i<29; ++i){
    for (j=0; j<27; ++j){
        
        listaobj[i][j] = 0 ; // Suelo por defecto
        
        if (j==3 && i>2 && i<26) // Pared por defecto
            listaobj[i][j] = 1 ;
        else if (j==23 && i>2 && i<26)
            listaobj[i][j] = 1 ;
        else if (j>2 && j<24){
            listaobj[3][j] = 1 ;
            listaobj[25][j] = 1 ;
        }
    }
}



function Cuadro(numero, posx, posy, imagen, pintor, titulo){
    
    this.numero = numero ;
    this.posx = posx ;
    this.posy = posy ;
    this.imagen = imagen ;
    this.pintor = pintor ;
    this.titulo = titulo ;
    
    this.getposx = function(){return this.posx ;} ;
    this.getposy = function(){return this.posy ;} ;
    this.getnum = function(){return this.numero ;} ;
    this.geturl = function(){return this.imagen ;} ;
    this.getpintor = function(){return this.pintor ;} ;
    this.gettitulo = function(){return this.titulo ;} ;
    
    this.setcuadro = setcuadro ;
    this.seturl = seturl ;
    this.setnum = setnum ;
} 





function seturl(ruta){ 
    
    this.imagen = ruta ;
}


function setnum(num){ 
    
    this.numero = num ;
}

function setcuadro(num,cx,cy,ima, aut, nom){
    
        this.numero = num ;
        this.posx = cx ;
        this.posy = cy ;
        this.imagen = ima ;
        this.pintor = aut ;
        this.titulo = nom ;
}
    
function GetNumCuadro(x, y){
    
    var n = 0 ;
    
    for ( i=0 ; i<ncuadros ; ++i){
        if ( listaObr[i].getposx() == x && listaObr[i].getposy() == y )
            n = listaObr[i].getnum() ;
    }
    
    return n ;
} 


    
function GetUrlCuadro(x, y){
    
    var url = 0 ;
    
    for ( i=0 ; i<ncuadros ; ++i){
        if ( listaObr[i].getposx() == x && listaObr[i].getposy() == y )
            url = listaObr[i].geturl() ;
    }
    return url ;
} 


function BorrarCuadro(x, y){
    
    var stop = false ;
    
    for ( i=0 ; i<ncuadros && stop==false ; i++){
        
        if ( listaObr[i].getposx() == x && listaObr[i].getposy() == y )            
            stop = true ;
    }
    
    if ( stop == true ){
             
        i-- ;
        
        listaObr.splice(i,1);  
        
        ncuadros-- ; 
        
        fin = false ;
        
        for ( j=i ; j<ncuadros; j++)
            listaObr[j].setnum(j+1);
    }
    
    PintaRejilla();
    
} 


        

//Recibe un identificador del elemento canvas y carga el canvas
//Devueve el contexto del canvas o FALSE si no ha podido conseguise
function cargaContextoCanvas(idCanvas){
    
   var elemento = document.getElementById(idCanvas);
   
   if(elemento && elemento.getContext){
       
      var contexto = elemento.getContext('2d');
      
      if(contexto){
         return contexto;
      }
   }
   return false;
}


function CargaCanvas(){
    
    //Recibimos el elemento canvas
    var contexto = cargaContextoCanvas('micanvas');
    
    //Si tengo el contexto 2d es que todo ha ido bien y puedo empezar a dibujar
    if (contexto) {
        
        //limpio el canvas
        contexto.clearRect(0,0,contexto.canvas.width,contexto.canvas.height);

        /***************DIBUJO DE LA PALETA***************/
        
        // PALETA BLANCA
        contexto.fillStyle = '#FFFFFF';
        contexto.beginPath();
        contexto.moveTo(2,2);
        contexto.lineTo(672,2);
        contexto.lineTo(672,626);
        contexto.lineTo(2,626);
        contexto.closePath();
        contexto.fill();      
        
        // BORDE MARCO
        contexto.strokeStyle = '#4E3609';
        contexto.lineWidth = 2;
        contexto.beginPath();
        contexto.moveTo(1,1);
        contexto.lineTo(673,1);
        contexto.lineTo(673,627);
        contexto.lineTo(1,627);
        contexto.closePath();
        contexto.stroke();
        
        PintaRejilla() ;

   }
}


function PintaRejilla(){
    
    //Recibimos el elemento canvas
    var contexto = cargaContextoCanvas('micanvas');
    
    //Si tengo el contexto 2d es que todo ha ido bien y puedo empezar a dibujar
    if (contexto) {

        for (f=0 ; f<29 ; ++f){
            for (c=0 ; c<27 ; ++c){

                if ( listaobj[f][c]==0 ) // SUELO                    
                    contexto.fillStyle = '#DEE1D5';
                else if ( listaobj[f][c]==1 ) // PARED
                    contexto.fillStyle = '#5F4235';//'#444B35';
                else if ( listaobj[f][c]==2 ) // PUERTA
                    contexto.fillStyle = '#EDCC97';
                else if ( listaobj[f][c]==3 ) // VENTANA
                    contexto.fillStyle = '#FFEA4D';
                else if ( listaobj[f][c]==4 ){ // OBRA
                    contexto.fillStyle = '#91A81F';//'#C3B022';
                }

                contexto.fillRect(f*23+5,c*23+5,20,20);
                
                if ( listaobj[f][c]==4 && ncuadros>0 ){
                    
                    var pos = GetNumCuadro(f,c) ;
                    
                    contexto.fillStyle = '#333333'; // Color del texto
                    contexto.textBaseline = "top"; // L�nea base del texto
                    
                    if ( pos < 10 ){
                        contexto.font = '14px Verdana'; // Tama�o y estilo de la fuente
                        contexto.fillText(pos,f*23+10,c*23+9);
                    }
                    else if (pos < 100 ){
                        contexto.font = '12px Verdana';
                        contexto.fillText(pos,f*23+7,c*23+10);
                    } 
                    else{
                        contexto.font = '10px Verdana';
                        contexto.fillText(pos,f*23+4,c*23+10);
                    }
                }
            }
        }
    }
}



function CargarObjeto(idBoton){
    
    if ( $('#panel').is (':visible') )
        $('#panel').fadeOut();
    
    idObj = idBoton ; 
    
    var para = true ;    
    
    $(document).ready(
        function(){
            $("#micanvas").mouseup(            
                function(evt){
                    evt.preventDefault();

                    para=true;
                })
        })


    $(document).ready(
        function(){
            $("#micanvas").mousedown(
                function(evt){
                    evt.preventDefault();   

                    para = false ;
                });
        })


    $(document).ready(
        function(){
            $("#micanvas").mousemove(
                function(evt){

                    evt.preventDefault();

                    var x = evt.pageX - $("#micanvas").offset().left;
                    var y = evt.pageY - $("#micanvas").offset().top ;

                    var fila = parseInt(x/23) ;
                    var col = parseInt(y/23) ;                

                    if ( para == true ){

                        CargaCanvas(); 
                        var contexto = cargaContextoCanvas('micanvas');

                        if (contexto) {                        

                            contexto.fillStyle = '#91DADE';
                            contexto.fillRect(fila*23+5,col*23+5,20,20);
                        }                        
                    }
                    else{
                        var check = Comprueba(fila,col,idObj);                       

                        if ( check == true ){

                            if ( idObj == "suelo")
                                listaobj[fila][col] = 0 ;
                            else if ( idObj == "pared")
                                listaobj[fila][col] = 1 ;
                            else if ( idObj == "puerta")
                                listaobj[fila][col] = 2 ;
                            else if ( idObj == "ventana")
                                listaobj[fila][col] = 3 ;
                            else if ( idObj == "obra" && fin==false )
                                listaobj[fila][col] = 4 ;

                            PintaRejilla();
                            
                            if ( ncuadros == 20 )
                                fin = true ;
                        }
                        else
                            PintaError(fila,col);
                    }
                });
    })


    $(document).ready(
        function(){
            $("#micanvas").click(
                function(evt){
                    
                    evt.preventDefault();

                    var x = evt.pageX - $("#micanvas").offset().left;
                    var y = evt.pageY - $("#micanvas").offset().top ;

                    var fila = parseInt(x/23) ;
                    var col = parseInt(y/23) ;

                    var check = Comprueba(fila,col,idObj);   
                                        
                    if ( check == true ){

                        if ( idObj == "suelo")
                            listaobj[fila][col] = 0 ;
                        else if ( idObj == "pared")
                            listaobj[fila][col] = 1 ;
                        else if ( idObj == "puerta")
                            listaobj[fila][col] = 2 ;
                        else if ( idObj == "ventana")
                            listaobj[fila][col] = 3 ;
                        else if ( idObj == "obra" && fin==false )
                            listaobj[fila][col] = 4 ;

                        PintaRejilla();
                        
                        if ( ncuadros == 20 )
                            fin = true ;
                    }
                    else
                        PintaError(fila,col);
                });
        })
}


function PintaError(x,y){
    
    CargaCanvas() ;
    var contexto = cargaContextoCanvas('micanvas');

    //Si tengo el contexto 2d es que todo ha ido bien y puedo empezar a dibujar
    if (contexto) {

        contexto.fillStyle = '#FE6C6C';
        contexto.fillRect(x*23+5,y*23+5,20,20);

        contexto.strokeStyle = '#E40101';
        contexto.lineWidth = 2;
        
        contexto.beginPath();
        contexto.moveTo(x*23+5,y*23+5);
        contexto.lineTo(x*23+25,y*23+5);
        contexto.lineTo(x*23+25,y*23+25);
        contexto.lineTo(x*23+5,y*23+25);
        contexto.closePath();
        
        contexto.moveTo(x*23+5,y*23+5);
        contexto.lineTo(x*23+25,y*23+25);
        
        contexto.moveTo(x*23+25,y*23+5);
        contexto.lineTo(x*23+5,y*23+25);
        
        contexto.stroke();
    }
}


function Comprueba (x,y,objeto){
    
    var ok = true ;
    
    if ( objeto == 'pared' && listaobj[x][y]!= 0 )
        ok = false ;
    
    else if ( objeto == 'ventana' ){ // Si es una ventana
        
        // Debe colocarse sobre un muro, no dando error si ya hay una ventana anteriormente
        if ( listaobj[x][y] != 1  && listaobj[x][y]  != 3 )
            ok = false ;
        if ( listaobj[x][y]==1 && ( listaobj[x+1][y]==4 || listaobj[x-1][y]==4 || listaobj[x][y+1]==4 || listaobj[x][y-1]==4 ) )
            ok = false ;
    }    
    else if ( objeto == 'puerta' ){ // Si es una ventana
        
        // Debe colocarse sobre un muro, no dando error si ya hay una puerta anteriormente
        if ( listaobj[x][y] != 1  && listaobj[x][y]  != 2 )
            ok = false ;
        if ( listaobj[x][y]==1 && ( listaobj[x+1][y]==4 || listaobj[x-1][y]==4 || listaobj[x][y+1]==4 || listaobj[x][y-1]==4 ) )
            ok = false ;
    }    
    else if ( objeto == 'obra' ){ // Si es una obra     
        
        if ( listaobj[x][y] != 0 && listaobj[x][y] != 4 ) // No debe estar sobre muro, puerta o ventana
            ok = false ;
        else if ( listaobj[x][y] == 0 ){ // Si ya hay un cuadro no lo exploramos, pero si hay suelo:
            
            var num = 0 ;
            
            if ( x==0 || y==0 || x==28 || y==26 ) // Los bordes no son v�lidos
                ok = false ;
            
            if ( x<28 && listaobj[x+1][y] != 1 )
                num++ ;
            if ( y<26 && listaobj[x][y+1] != 1 )
                num++ ;     
            if ( x>0 && listaobj[x-1][y] != 1 )
                num++ ;     
            if ( y>0 && listaobj[x][y-1] != 1 ) 
                num++ ;
            
            if ( num == 4 ) // Debe tener un muro a su izquierda, derecha, delante o detr�s  
                ok = false ;
        }
    }
    else if ( objeto == 'suelo' ){
        if ( listaobj[x][y] == 4 )
            BorrarCuadro(x,y);
    }
    
    if ( objeto == 'ventana' || objeto == 'puerta' || objeto == 'obra' ){ // Sin esquinas 
        
        if ( ( x==0 || x==28 ) && ( y==0 || y==26 ) )
            ok = false ;
        
        if ( x==0 ){
            if ( listaobj[x+1][y] == 1 ){
                
                if ( y>0 && y<26 && ( listaobj[x][y+1] == 1  || listaobj[x][y-1] == 1 ) ) 
                    ok = false ;
                if ( y==0 && listaobj[x][y+1] == 1 )
                    ok = false ;
                if ( y==26 && listaobj[x][y-1] == 1 )
                    ok = false ;
            }                
        }
        if ( x==28 ){
            if ( listaobj[x-1][y] == 1 ){
                
                if ( y>0 && y<26 && ( listaobj[x][y+1] == 1  || listaobj[x][y-1] == 1 ) ) 
                    ok = false ;
                if ( y==0 && listaobj[x][y+1] == 1 )
                    ok = false ;
                if ( y==26 && listaobj[x][y-1] == 1 )
                    ok = false ;
            }                
        }
        if ( x>0 && x<28 && y>0 && y<26 ){
                
            if ( ( listaobj[x-1][y] == 1  || listaobj[x+1][y] == 1 ) && ( listaobj[x][y+1] == 1 || listaobj[x][y-1] == 1 ) )
                ok = false ;
        }
    }
    
    
    if ( ncuadros < 20 && ok == true && objeto == 'obra' && listaobj[x][y] != 4 ){ // Si no exist�a ya de antes, creamos una nueva obra
  
        listaObr[ncuadros] = new Cuadro(ncuadros+1,x,y,null);
        ncuadros++ ; 
        
        if ( ncuadros == 20 )
            alert("El n\u00famero m\u00e1ximo de cuadros es 20.");
    } 

    return ok ;
}



function CargarImagenes(){

    
    if ( ncuadros>0 ){
        
        CreaGuardado(1) ;       
        
        var html = "<form action='museo/subeima.php' method='post' enctype='multipart/form-data'>";
        html += "<nav id='carga'>" ;
        
        for ( i=1 ; i<=ncuadros; i++ ){
            html += "<p>Imagen " + i + "</p><ul><li><label for='pintor" + i + "' id='c1'>Nombre del artista:</label><input type='text' name='pintor" + i + "' id='pintor" + i + "' maxlength='50'/></li>" ;
            html += "<li><label for='nombreobra" + i + "' id='c2'>T&iacute;tulo de la obra:</label><input type='text' name='nombreobra" + i + "' id='nombreobra" + i + "' maxlength='50'/>" ;
            html += "<li><label for='ima" + i + "' id='c3'>Fotograf&iacute;a:</label><input type='file' name='ima" + i + "' id='ima" + i + "' /></li></ul>" ;
        }

        html += "<input type='submit' class='boton4' name='okcarga' id='okcarga' value='Aceptar'/><button id='cancelar' class='boton4' title='Salir'>Cancelar</button>" ;
        html += "<input type='hidden' id='nima' name='nima' value='" + ncuadros + "'/>" ;
        html += "</nav></form>" ;

        $('#panel').html(html);
             
        
        $(document).ready(
            function(){
                $("#cancelar").click(
                    function(evento){
                        evento.preventDefault();

                         var pregunta = window.confirm("\u00BFQuiere salir sin guardar?");

                         // Si acepta salimos sin guardar
                         // Si cancela vuelve al explorador de im�genes
                         if ( pregunta )
                            $('#panel').fadeOut();
                    });
            })  
    }
    else
        alert("Antes debe insertar alguna obra en la paleta.");
}



function Panel(){
    
    if ( ($('#panel').is (':hidden')) && ncuadros>0 )
        $('#panel').show('slow');
}


function CreaGuardado(tipo){
        
    var pos = 0 ;    
    var malla = {} ;
        
    for (f=0 ; f<29 ; ++f){
        for (c=0 ; c<27 ; ++c){

            malla[pos] = listaobj[f][c] ;
            pos++ ;
        }
    }

    if (tipo == 1){
        
        var cuadros = {} ;
        
        pos = 0 ;
        cuadros[pos] = ncuadros ;
        pos++ ; 
        
        for ( p=0 ; p<ncuadros; p++ ){

            cuadros[pos] = listaObr[p].geturl() ;
            pos++ ;
            cuadros[pos] = listaObr[p].getposx() ;
            pos++ ;
            cuadros[pos] = listaObr[p].getposy() ;
            pos++ ;
            cuadros[pos] = listaObr[p].getpintor() ;
            pos++ ;
            cuadros[pos] = listaObr[p].gettitulo() ;
            pos++ ;
        }
        
        $.post("museo/guardar_imags.php", cuadros, null );
        
        $.post("museo/guardar_museo.php", malla, null );        
    }
    
    
    else if (tipo == 2){       
        
        $.post("museo/guardar_museo.php", malla, 
            function(datos) {              
              
              if ( datos.length > 0 ){
                
                if ( datos == 1 )
                    alert('Museo guardado satisfactoriamente.')
                else if ( datos == 2 )
                    alert('Error al guardar su dise\u00f1o. Int\u00e9ntelo de nuevo.');                
              }
              
            } );        
    }
}
                

function Cancela(){
    
    var preg = window.confirm("\u00BFEs este su dise\u00f1o definitivo?");
    
    // Si acepta guardamos el museo
    // Si cancela salimos sin guardar
    
    if ( preg ){
        
        window.alert("El proceso tardar\u00e1 unos segundos. Gracias");
        
        CreaGuardado(2) ;
    }        
}


function MuseoAnterior(){
    
    $.get("museo/cargar.php", null,
    
          function(datos) {              
              
              if ( datos.length > 0 ){
                  
                  var pos = 0 ;
                  var n = 0 ;

                  var temp = new Array();
                  temp = datos.split(',');

                  for (f=0 ; f<29 ; ++f){
                    for (c=0 ; c<27 ; ++c){ 

                        listaobj[f][c] = temp[pos] ;

                        if (listaobj[f][c]==4)
                            n++ ;

                        pos++ ;
                    }
                  }

                  for (p=0 ; p<n ; p++ ){

                      listaObr[p] = new Cuadro(p+1,temp[pos+1],temp[pos+2],temp[pos],temp[pos+3],temp[pos+4]);
                      
                      if ( temp[pos+1] != "" )
                          nimag++ ;
                          
                      pos = pos + 5 ;
                  }
                  ncuadros = n ;              

                  CargaCanvas() ;
              }
          });
      
      CargaCanvas() ;
}


function Revisa(){
    
    if ( ncuadros>0 && nimag>0 )
        $("#paleta").load("museo/laberinto.php");
    else
        alert("Error, antes debe crear cuadros y cargarlos con sus respectivas im\u00e1genes.");

}



function CreaMundo(){
    
    var x = 29 ;
    var z = 27 ;
    var ancho = (-x/2)+28*1.5 ;
    var alto = (-z/2)+26*1.5 ;
        
   var museo = {       
       
       "suelo":{
           "texturas": [
                   0.0, 1.0,
                   0.0, 0.0,
                   1.0, 0.0,

                   0.0, 1.0,
                   1.0, 1.0,
                   1.0, 0.0
               ],
           "vertices": [
                   -x/2, 0.0, -z/2,
                   -x/2, 0.0,  alto,
                    ancho, 0.0,  alto, 

                   -x/2, 0.0, -z/2,
                    ancho, 0.0, -z/2,
                    ancho, 0.0,  alto                            
               ],
           "numfilas": 6
       },
       "pared":{
           "texturas": [],
           "vertices": [],
           "numfilas": 0
       },
       "puerta":{
           "texturas": [],
           "vertices": [],
           "numfilas": 0
       },
       "ventana":{
           "texturas": [],
           "vertices": [],
           "numfilas": 0
       },
       "obra":{
           "texturas": new Array(ncuadros),
           "vertices": new Array(ncuadros),
           "foto": [],
           "tama": ncuadros
       },
       "techo":{
           "texturas": [],
           "vertices": [],
           "numfilas": 0
       }
   };

    var xd, zd ;
    
    var f ;
    for (f=0 ; f<29 ; ++f){
        
        var inicio = -1 ;
        var fin = -1 ;
        
        for (c=0 ; c<27 && inicio == -1 ; ++c)
            if ( listaobj[f][c] != "0")
                inicio = c ;
        
        for (c=26 ; c>=0 && fin == -1 ; --c)
            if ( listaobj[f][c] != "0")
                fin = c ;
            
        if ( inicio != -1 ){
                    
            xd = (-x/2)+f*1.44+0.85 ;  
            
            
            for ( i=inicio ; i<fin; i++ ){
                                
                zd = (-z/2)+i*1.5+0.7 ;
                
                if ( i < fin-1 ){

                    museo.techo.vertices.push(
                             xd, 1.5,  zd,
                             xd, 1.5,  zd+1.5,
                         1.44+xd, 1.5,  zd+1.5,

                             xd, 1.5,  zd,
                         1.44+xd, 1.5,  zd,
                         1.44+xd, 1.5,  zd+1.5 ) ;                    
                }
                else{
                    museo.techo.vertices.push(
                                 xd, 1.5,  zd,
                                 xd, 1.5,  zd+1.6,
                             1.44+xd, 1.5,  zd+1.6,

                                 xd, 1.5,  zd,
                             1.44+xd, 1.5,  zd,
                             1.44+xd, 1.5,  zd+1.6 ) ;
                }
                
                museo.techo.texturas.push(
                   0.0, 1.0,
                   0.0, 0.0,
                   1.0, 0.0,

                   0.0, 1.0,
                   1.0, 1.0,
                   1.0, 0.0 ) ;

                museo.techo.numfilas += 6 ;            
                
            }
        }
        
    }
    
    var pos = 0 ;

     for (f=0 ; f<29 ; ++f){
        for (c=0 ; c<27 ; ++c){
            
            if ( listaobj[f][c] != "0"){
                
                xd = (-x/2)+f*1.5 ;
                zd = (-z/2)+c*1.5 ;

                if ( listaobj[f][c] == "1"){
                                                
                        museo.pared.vertices.push(
                             xd, 1.5,  zd,
                             xd, 0.0,  zd,
                         1.5+xd, 0.0,  zd,
                             xd, 1.5,  zd,
                         1.5+xd, 1.5,  zd,
                         1.5+xd, 0.0,  zd,
                         
                             xd, 1.5,  1.5+zd,
                             xd, 0.0,  1.5+zd,
                         1.5+xd, 0.0,   1.5+zd,
                             xd, 1.5,  1.5+zd,
                         1.5+xd, 1.5,   1.5+zd,
                         1.5+xd, 0.0,   1.5+zd,
                         
                          xd, 1.5,  zd,
                             xd, 0.0,  zd,
                         xd, 0.0,  1.5+zd,
                             xd, 1.5,  zd,
                         xd, 1.5,  1.5+zd,
                         xd, 0.0,  1.5+zd,
                         
                             1.5+xd, 1.5,  zd,
                             1.5+xd, 0.0,  zd,
                         1.5+xd, 0.0,   1.5+zd,
                             1.5+xd, 1.5,  zd,
                         1.5+xd, 1.5,   1.5+zd,
                         1.5+xd, 0.0,   1.5+zd
                     ) ;
                    
                             
                    museo.pared.texturas.push(
                       0.0, 1.0,
                       0.0, 0.0,
                       1.0, 0.0,
                       0.0, 1.0,
                       1.0, 1.0,
                       1.0, 0.0,
                       
                       0.0, 1.0,
                       0.0, 0.0,
                       1.0, 0.0,
                       0.0, 1.0,
                       1.0, 1.0,
                       1.0, 0.0,
                       
                       0.0, 1.0,
                       0.0, 0.0,
                       1.0, 0.0,
                       0.0, 1.0,
                       1.0, 1.0,
                       1.0, 0.0,
                   
                       0.0, 1.0,
                       0.0, 0.0,
                       1.0, 0.0,
                       0.0, 1.0,
                       1.0, 1.0,
                       1.0, 0.0) ;
                       
                    museo.pared.numfilas += 24 ;
                }
                else{
                    
                    
                    
                    if ( listaobj[f][c] == "2"){
                    
                        if ( listaobj[f][c+1] != "0" || listaobj[f][c-1] != "0" ){
                            
                             xd += 0.75 ;
                             
                             museo.puerta.vertices.push(
                                 xd, 1.5,  zd,
                                 xd, 0.0,  zd,
                                 xd, 0.0,  zd+1.5,

                                 xd, 1.5,  zd,
                                 xd, 1.5,  zd+1.5,
                                 xd, 0.0,  zd+1.5 ) ;
                        }
                        else{
                            
                            zd += 0.75 ;

                            museo.puerta.vertices.push(
                                 xd, 1.5,  zd,
                                 xd, 0.0,  zd,
                             1.5+xd, 0.0,  zd,

                                 xd, 1.5,  zd,
                             1.5+xd, 1.5,  zd,
                             1.5+xd, 0.0,  zd ) ;
                        }

                        museo.puerta.texturas.push(
                           0.0, 1.0,
                           0.0, 0.0,
                           1.0, 0.0,

                           0.0, 1.0,
                           1.0, 1.0,
                           1.0, 0.0 ) ;

                        museo.puerta.numfilas += 6 ;
                    }
                    else if ( listaobj[f][c] == "3"){

                        if ( ( listaobj[f][c+1] != "0" && listaobj[f][c+1] != "4" ) || ( listaobj[f][c-1] != "0" && listaobj[f][c-1] != "4" )  ){

                             xd += 0.75 ;
                             
                             museo.ventana.vertices.push(
                                 xd, 1.5,  zd,
                                 xd, 0.0,  zd,
                                 xd, 0.0,  zd+1.5,

                                 xd, 1.5,  zd,
                                 xd, 1.5,  zd+1.5,
                                 xd, 0.0,  zd+1.5 ) ;
                        }
                        else{

                            zd += 0.75 ;
                            
                            museo.ventana.vertices.push(
                                 xd, 1.5,  zd,
                                 xd, 0.0,  zd,
                             1.5+xd, 0.0,  zd,

                                 xd, 1.5,  zd,
                             1.5+xd, 1.5,  zd,
                             1.5+xd, 0.0,  zd ) ;
                        }

                        museo.ventana.texturas.push(
                           0.0, 1.0,
                           0.0, 0.0,
                           1.0, 0.0,

                           0.0, 1.0,
                           1.0, 1.0,
                           1.0, 0.0 ) ;

                        museo.ventana.numfilas += 6 ;
                    }
                    else if ( listaobj[f][c] == "4"){
                        
                        
                        var aux ;

                        if ( listaobj[f][c-1] == "1" || listaobj[f][c+1] == "1" ){

                            if ( listaobj[f][c-1] == "1" )
                                zd += 0.05 ;
                            else
                                zd += 1.45 ;

                            aux = [
                             0.2+xd, 1.2,  zd,
                             0.2+xd, 0.4,  zd,
                             1.0+xd, 0.4,  zd,

                             0.2+xd, 1.2,  zd,
                             1.0+xd, 1.2,  zd,
                             1.0+xd, 0.4,  zd ] ;
                         
                            museo.obra.vertices[pos] = aux ;
                            
                        }
                        else{
                            if ( listaobj[f-1][c] == "1" )
                                xd += 0.05 ;
                            else
                                xd += 1.45 ;

                            
                             aux = [
                             xd, 1.2,  0.2+zd,
                             xd, 0.4,  0.2+zd,
                             xd, 0.4,  1.0+zd,

                             xd, 1.2,  0.2+zd,
                             xd, 1.2,  1.0+zd,
                             xd, 0.4,  1.0+zd ] ; 
                         
                            museo.obra.vertices[pos] = aux ;
                        }
                        
                        aux = [
                           0.0, 1.0,
                           0.0, 0.0,
                           1.0, 0.0,

                           0.0, 1.0,
                           1.0, 1.0,
                           1.0, 0.0 ] ;
                       
                       museo.obra.texturas[pos] = aux ;
                        
                        
                        // Le ponemos el cuadro
                        
                        var path = GetUrlCuadro(f,c) ;
                        
                        if ( path != 0 )
                            museo.obra.foto.push(path) ;        
                        
                        pos++ ;
                    }
                }
            }
        }
     }
     
    return museo ;
}







function HazListado(){    
    
    if ( ncuadros > 0 ){
        
        var html = "<nav id='panelobras'>" ;
        
        for ( i=1 ; i<=ncuadros; i++ ){            
            html += "<figure id='columizq'><img src='"+ listaObr[i-1].geturl() +"' ></img></figure>"
            html += "<section id='columder'>" ;
            html += "<p>Cuadro n&deg; " + i + "</p>" ;
            html += "<ul><li><label>Autor de la obra: </label><p>" + listaObr[i-1].getpintor() ;
            html += "</p></li><li><label>T&iacute;tulo: </label><p>" + listaObr[i-1].gettitulo() ;
            html += "<p></ul></section>"
        }

        $('#lista').html(html);        
    }
}     