
// "gl" ser� la variable con la que llamemos a las funciones de OpenGl
var gl;

// Shader -> es una tecnolog�a en un lenguaje de sombreado que se puede compilar independientemente.
/* PASOS:
 - Vertexshader -> opera con los v�rtices actuales y guarda los resultados en las variables "varying"
 (como "glPosition" que tiene las coordenadas de los v�rtices).
 - �stas se convierten en una imagen 2D.
 - Fragment shader -> Devuelve el color de los puntos de interpolaci�n entre los v�rtices,
 este se guarda en la varying "glColor".
 - El resultado se pasa al Frame buffer (se lo env�a a la tarjeta gr�fica y se pinta la escena).
*/
var shaderProgram;


/* Variable para definir la identidad, asociarle los shaders de dibujo
  y realizar operaciones como:
  - Multiplicaci�n (Escalado).
  - Rotaci�n.
  - Traslaci�n.
*/
var mvMatrix = mat4.create();

// Matriz que representa la pila de operaciones
var mvMatrixStack = [];

// Variable para definir la perspectiva y asociarle los shaders de dibujo
var pMatrix = mat4.create();


var currentlyPressedKeys = {};

// Para animar la escena
var lastTime = 0;

var pitch = 0;
var pitchRate = 0;

var yaw = 0;
var yawRate = 0;

var xPos = 7.25;
var yPos = 1;
var zPos = 27.9;

var speed = 0;

// Simula saltos cuando corremos hacia adelante.
var joggingAngle = 0;


// Formato del archivo de coordenadas
// n� total de tri�ngulos
// coordenadas (x,y,z) para la posici�n y coordenadas (s,t) para la textura
var sueloVertexPositionBuffer = null;
var sueloVertexTextureCoordBuffer = null;
var paredVertexPositionBuffer = null;
var paredVertexTextureCoordBuffer = null;
var puertaVertexPositionBuffer = null;
var puertaVertexTextureCoordBuffer = null;
var ventanaVertexPositionBuffer = null;
var ventanaVertexTextureCoordBuffer = null;
var techoVertexPositionBuffer = null;
var techoVertexTextureCoordBuffer = null;
var obraVertexPositionBuffer = {};
var obraVertexTextureCoordBuffer = {};


var sueloTexture;
var paredTexture;
var puertaTexture;
var ventanaTexture;
var techoTexture;


var ima1;
var ima2;
var ima3;
var ima4;
var ima5;
var ima6;
var ima7;
var ima8 ;
var ima9 ;
var ima10 ;
var ima11 ;
var ima12 ;
var ima13 ;
var ima14 ;
var ima15 ;
var ima16 ;
var ima17 ;
var ima18 ;
var ima19 ;
var ima20 ;

var mundo ;

var primero=false;
var segundo=false;

var texturesToLoad;



function initGL(canvas) {
    
    try {
        // Cargamos en "gl" el contexto para pintar
        gl = canvas.getContext("experimental-webgl");
        
        // Guardamos el tama�o del lienzo
        gl.viewportWidth = canvas.width;
        gl.viewportHeight = canvas.height;
    } catch (e) {}
    if (!gl) 
        alert("Lo sentimos, no se ha podido inicializar webGL");
}



function getShader(gl, id) {
    
    var shaderScript = document.getElementById(id);
    if (!shaderScript) {
        return null;
    }

    var str = "";
    var k = shaderScript.firstChild;
    while (k) {
        if (k.nodeType == 3)
            str += k.textContent;
        k = k.nextSibling;
    }

    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    }
    else
        return null;

    gl.shaderSource(shader, str);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}


function initShaders() {
    
    var fragmentShader = getShader(gl, "shader-fs");
    var vertexShader = getShader(gl, "shader-vs");

    shaderProgram = gl.createProgram();
    
     /* vertexShader:
      Herramienta de OpenGl que trabaja con los v�rtices de los modelos 3D:
      - Mediante operaciones matem�ticas sobre las variables.
      - Definiendo colores, texturas e incidencia de la luz.
    */
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
        alert("No se pudieron inicializar los shaders");

    gl.useProgram(shaderProgram);

    // Asociamos la POSICI�N
    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

    // Asociamos la TEXTURA
    shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
    gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

    // Asociamos la matriz de PERSPECTIVA
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
    
    // Asociamos la matriz de IDENTIDAD
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
    shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
}






function mvPushMatrix() {
    
    // Simulaci�n matriz PushMatrix de OpenGl
    // para a�adir en el tope de la pila una operaci�n
    var copy = mat4.create();
    mat4.set(mvMatrix, copy);
    mvMatrixStack.push(copy);
}

function mvPopMatrix() {
    
    // Simulaci�n matriz PopMatrix de OpenGl
    // para quitar del tope de la pila una operaci�n
    if (mvMatrixStack.length == 0) {
        throw "Invalid popMatrix!";
    }
    mvMatrix = mvMatrixStack.pop();
}


function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}


function degToRad(degrees) {
    
    // Funci�n para rotar pasando los grados a radianes (Grados x PI/180�)
    return degrees * Math.PI / 180;
}




function handleKeyDown(event) {
    
    currentlyPressedKeys[event.keyCode] = true;
}


function handleKeyUp(event) {
    
    currentlyPressedKeys[event.keyCode] = false;
}



function handleKeys() {
    
    pitchRate = 0 ;
    
    // Cursores flechas o videojuego
    if (currentlyPressedKeys[65]) {
      // Left cursor key or A
      yawRate = 0.1;
    }
    else if (currentlyPressedKeys[68]) {
      // Right cursor key or D
      yawRate = -0.1;
    }
    else
      yawRate = 0;
    

    if (currentlyPressedKeys[87]) {
      // Up cursor key or W
      speed = 0.003;
    }
    else if (currentlyPressedKeys[83]) {
      // Down cursor key
      speed = -0.003;
    }
    else
      speed = 0;
}






function animate() {
    
    // Animaci�n en la que el cubo gira a Speed� por minuto.
    var timeNow = new Date().getTime();
    
    if (lastTime != 0) {
        
        var elapsed = timeNow - lastTime;

        if (speed != 0) {
            
            xPos -= Math.sin(degToRad(yaw)) * speed * elapsed;
            zPos -= Math.cos(degToRad(yaw)) * speed * elapsed;

            joggingAngle += elapsed * 0.6;  // Para simular los saltitos
            yPos = Math.sin(degToRad(joggingAngle)) / 20 + 0.7;
            
        }
        
        yaw += yawRate * elapsed;
        pitch += pitchRate * elapsed;
    }
    lastTime = timeNow;
}


function tick() {
    
    requestAnimFrame(tick); // Repintamos
    
    handleKeys();
    drawScene();
    animate(); 
}


function webGLStart() {
    
    var canvas = document.getElementById("dibujo");
    
    // Comprobamos si el navegador soporta Webgl
    initGL(canvas);
    
    // Inicializamos los Shaders y la textura
    initShaders();  
    
    // Cargamos el mundo
    loadWorld();
    
    initTexture();
    initCuadros();
    
    // Inicializamos la escena     
    // Borramos la escena con:
    // blanco: 0.0, 0.0, 0.0
    // Transparente: 0.0 / Opaco: 1.0
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.enable(gl.DEPTH_TEST);

    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

    tick();
        
    
}

function handleLoadedTexture(texture) {
    
    // La colocamos en posici�n vertical
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

    // Asociamos la textura plana 2D
    gl.bindTexture(gl.TEXTURE_2D, texture);
    
    // Se la pasamos a la gr�fica
    // par�metros (tipo de imagen, nivel de detalle, formato a representar, imagen a cargar)
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
    
    // Funciones de escalado
    // La 1� escala cuando la textura en la escena es mayor que la original
    // La 2� cuando es menor la original que la que se pinta
    if (isPowerOfTwo(texture.width) && isPowerOfTwo(texture.height)){
        
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
        gl.generateMipmap(gl.TEXTURE_2D);
    }
    else{
        
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    }

    // Ponemos el bind a null para asociar futuras texturas
    gl.bindTexture(gl.TEXTURE_2D, null);
}


function isPowerOfTwo(value) {
    
    return ((value & (value - 1)) == 0);
}



function initTexture(){
    
    sueloTexture = gl.createTexture();
    
    sueloTexture.image = new Image();
    
    sueloTexture.image.onload = function (){
        handleLoadedTexture(sueloTexture);
    };

    sueloTexture.image.src = "images/suelo.png";
    
    
    
    
   paredTexture = gl.createTexture();
    
    paredTexture.image = new Image();
    
    paredTexture.image.onload = function (){
        handleLoadedTexture(paredTexture);
    };

    paredTexture.image.src = "images/pared.png";
    
    
    
    
    puertaTexture = gl.createTexture();
    
    puertaTexture.image = new Image();
    
    puertaTexture.image.onload = function (){
        handleLoadedTexture(puertaTexture);
    };

    puertaTexture.image.src = "images/MarcoPuerta.png";
    
    
    
    
    ventanaTexture = gl.createTexture();
    
    ventanaTexture.image = new Image();
    
    ventanaTexture.image.onload = function (){
        handleLoadedTexture(ventanaTexture);
    };

    ventanaTexture.image.src = "images/MarcoVentana.png";
    
    
    
    
    techoTexture = gl.createTexture();
    
    techoTexture.image = new Image();
    
    techoTexture.image.onload = function (){
        handleLoadedTexture(techoTexture);
    };

    techoTexture.image.src = "images/Techo.png";     
}






function handleLoadedWorld(data) {
    
    var vertexCount = 0;    

    /******************* SUELO ********************/
    
    vertexCount = data.suelo.numfilas ;    
    
    // V�rtices
    sueloVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, sueloVertexPositionBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.suelo.vertices), gl.STATIC_DRAW);
    
    sueloVertexPositionBuffer.itemSize = 3;
    sueloVertexPositionBuffer.numItems = vertexCount;
    

    // Texturas   
    
    sueloVertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, sueloVertexTextureCoordBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.suelo.texturas), gl.STATIC_DRAW);
    
    sueloVertexTextureCoordBuffer.itemSize = 2;
    sueloVertexTextureCoordBuffer.numItems = vertexCount;
    
    
    
    
    


    /******************* PARED ********************/    
    
    vertexCount = data.pared.numfilas ;
    
    // V�rtices
    paredVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, paredVertexPositionBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.pared.vertices), gl.STATIC_DRAW);
    
    paredVertexPositionBuffer.itemSize = 3;
    paredVertexPositionBuffer.numItems = vertexCount;
    
    
    // Texturas    
    
    paredVertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, paredVertexTextureCoordBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.pared.texturas), gl.STATIC_DRAW);
    
    paredVertexTextureCoordBuffer.itemSize = 2;
    paredVertexTextureCoordBuffer.numItems = vertexCount;
    
    
    
    
    
    /******************* PUERTA ********************/
    
    vertexCount = data.puerta.numfilas ;
    
    // V�rtices
    puertaVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, puertaVertexPositionBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.puerta.vertices), gl.STATIC_DRAW);
    
    puertaVertexPositionBuffer.itemSize = 3;
    puertaVertexPositionBuffer.numItems = vertexCount;
    

    // Texturas   
    
    puertaVertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, puertaVertexTextureCoordBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.puerta.texturas), gl.STATIC_DRAW);
    
    puertaVertexTextureCoordBuffer.itemSize = 2;
    puertaVertexTextureCoordBuffer.numItems = vertexCount;
    
    
    
    
    
    
    
    /******************* VENTANA ********************/
    
    vertexCount = data.ventana.numfilas ;
    
    // V�rtices
    ventanaVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, ventanaVertexPositionBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.ventana.vertices), gl.STATIC_DRAW);
    
    ventanaVertexPositionBuffer.itemSize = 3;
    ventanaVertexPositionBuffer.numItems = vertexCount;
    

    // Texturas   
    
    ventanaVertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, ventanaVertexTextureCoordBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.ventana.texturas), gl.STATIC_DRAW);
    
    ventanaVertexTextureCoordBuffer.itemSize = 2;
    ventanaVertexTextureCoordBuffer.numItems = vertexCount;


    
    
    
    
    
    /******************* TECHO ********************/
    
    vertexCount = data.techo.numfilas ;
    
    // V�rtices
    techoVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, techoVertexPositionBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.techo.vertices), gl.STATIC_DRAW);
    
    techoVertexPositionBuffer.itemSize = 3;
    techoVertexPositionBuffer.numItems = vertexCount;
    

    // Texturas   
    
    techoVertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, techoVertexTextureCoordBuffer);
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.techo.texturas), gl.STATIC_DRAW);
    
    techoVertexTextureCoordBuffer.itemSize = 2;
    techoVertexTextureCoordBuffer.numItems = vertexCount;
    
    
    
    
    
    
    /******************* OBRA ********************/
    var b ;
    
    for ( b=0 ; b<mundo.obra.tama; b++ ){        
    
        vertexCount = 6 ;

        // V�rtices
        obraVertexPositionBuffer[b] = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, obraVertexPositionBuffer[b]);
        
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.obra.vertices[b]), gl.STATIC_DRAW);

        obraVertexPositionBuffer[b].itemSize = 3;
        obraVertexPositionBuffer[b].numItems = vertexCount;
        

        // Texturas   
        obraVertexTextureCoordBuffer[b] = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, obraVertexTextureCoordBuffer[b]);
        
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.obra.texturas[b]), gl.STATIC_DRAW);

        obraVertexTextureCoordBuffer[b].itemSize = 2;
        obraVertexTextureCoordBuffer[b].numItems = vertexCount;       
    }
    
    
    
    
    
    document.getElementById("cargando").textContent = "";
    
    
}


function loadWorld() {
    
    mundo = CreaMundo() ;
    texturesToLoad = mundo.obra.tama ;
    
    handleLoadedWorld(mundo);
}






function drawScene() {
    
    // Definimos una vista, con su tama�o, y la "limpiamos" para poder dibujar
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (sueloVertexTextureCoordBuffer == null || sueloVertexPositionBuffer == null
        || paredVertexTextureCoordBuffer == null || paredVertexPositionBuffer == null
        || puertaVertexTextureCoordBuffer == null || puertaVertexPositionBuffer == null
        || ventanaVertexTextureCoordBuffer == null || ventanaVertexPositionBuffer == null
        || techoVertexTextureCoordBuffer == null || techoVertexPositionBuffer == null){
      return;
    }
      
    // C�MARA
    // Definimos una perspectiva (para que los objetos tengan profundidad):
    // De 45�, con el ancho/alto del punto de vista.
    // Decidimos hasta que profundidad m�xima y m�nima queremos representar: 100 y 0.1  
    mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);
        
    //Creamos una matriz identidad (4x4)
    // Sirve para las operaciones con los objetos: traslaci�n, rotaci�n...
    mat4.identity(mvMatrix);

    // Simulamos el movimiento de la c�mara
    // pitch: Inclinaci�n sobre el eje X mirando hacia arriba/abajo
    // yaw: �ngulo sobre el eje Y girando hacia la izquierda/derecha
    mat4.rotate(mvMatrix, degToRad(-pitch), [1, 0, 0]);
    mat4.rotate(mvMatrix, degToRad(-yaw), [0, 1, 0]);
    
    // Cambiamos la posici�n del visor de la escena    
    mat4.translate(mvMatrix, [-xPos, -yPos, -zPos]);
    
    
    
    /********************** SUELO **********************/
    
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, sueloTexture);
    gl.uniform1i(shaderProgram.samplerUniform, 0);
    
    mvPushMatrix();

    gl.bindBuffer(gl.ARRAY_BUFFER, sueloVertexTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, sueloVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, sueloVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, sueloVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

    setMatrixUniforms();
    gl.drawArrays(gl.TRIANGLES, 0, sueloVertexPositionBuffer.numItems);
    
    mvPopMatrix();
    
    
    /********************** PARED **********************/
    mvPushMatrix();
    
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, paredTexture);
    gl.uniform1i(shaderProgram.samplerUniform, 1);

    gl.bindBuffer(gl.ARRAY_BUFFER, paredVertexTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, paredVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, paredVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, paredVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

    setMatrixUniforms();
    gl.drawArrays(gl.TRIANGLES, 0, paredVertexPositionBuffer.numItems);
    
    mvPopMatrix();
    
    
    /********************** PUERTA **********************/
    mvPushMatrix();
    
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, puertaTexture);
    gl.uniform1i(shaderProgram.samplerUniform, 2);

    gl.bindBuffer(gl.ARRAY_BUFFER, puertaVertexTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, puertaVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, puertaVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, puertaVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

    setMatrixUniforms();
    gl.drawArrays(gl.TRIANGLES, 0, puertaVertexPositionBuffer.numItems);
    
    mvPopMatrix();
    
    
    /********************** VENTANA **********************/
    mvPushMatrix();
    
    gl.activeTexture(gl.TEXTURE3);
    gl.bindTexture(gl.TEXTURE_2D, ventanaTexture);
    gl.uniform1i(shaderProgram.samplerUniform, 3);

    gl.bindBuffer(gl.ARRAY_BUFFER, ventanaVertexTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, ventanaVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, ventanaVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, ventanaVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

    setMatrixUniforms();
    gl.drawArrays(gl.TRIANGLES, 0, ventanaVertexPositionBuffer.numItems);
    
    mvPopMatrix();
    
  
    
    
    
    /********************** TECHO **********************/
    mvPushMatrix();
    
    gl.activeTexture(gl.TEXTURE4);
    gl.bindTexture(gl.TEXTURE_2D, techoTexture);
    gl.uniform1i(shaderProgram.samplerUniform, 4);

    gl.bindBuffer(gl.ARRAY_BUFFER, techoVertexTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, techoVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, techoVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, techoVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

    setMatrixUniforms();
    gl.drawArrays(gl.TRIANGLES, 0, techoVertexPositionBuffer.numItems);
    
    mvPopMatrix();
    
    
    
    /********************** OBRA **********************/
    
    var p ;
        
    for ( p=0 ; p<mundo.obra.tama; p++ ){         

        mvPushMatrix();

        gl.activeTexture(gl.TEXTURE5);

        var imax ;
        
        if ( p==0 )
            imax = ima1 ;
        else if ( p==1 )
            imax = ima2 ;
        else if ( p==2 )
            imax = ima3 ;
        else if ( p==3 )
            imax = ima4 ;
        else if ( p==4 )
            imax = ima5 ;
        else if ( p==5 )
            imax = ima6 ;
        else if ( p==6 )
            imax = ima7 ;
        else if ( p==7 )
            imax = ima8 ;
        else if ( p==8 )
            imax = ima9 ;        
        else if ( p==9 )
            imax = ima10 ;        
        else if ( p==10 )
            imax = ima11 ;        
        else if ( p==11 )
            imax = ima12 ;        
        else if ( p==12 )
            imax = ima13 ;        
        else if ( p==13 )
            imax = ima14 ;        
        else if ( p==14 )
            imax = ima15 ;        
        else if ( p==15 )
            imax = ima16 ;        
        else if ( p==16 )
            imax = ima17 ;        
        else if ( p==17 )
            imax = ima18 ;        
        else if ( p==18 )
            imax = ima19 ;        
        else if ( p==19 )
            imax = ima20 ;         
        
            
        gl.bindTexture(gl.TEXTURE_2D, imax);
        gl.uniform1i(shaderProgram.samplerUniform, 5);


        gl.bindBuffer(gl.ARRAY_BUFFER, obraVertexTextureCoordBuffer[p]);        
        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, obraVertexTextureCoordBuffer[p].itemSize, gl.FLOAT, false, 0, 0);

        
        gl.bindBuffer(gl.ARRAY_BUFFER, obraVertexPositionBuffer[p]);        
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, obraVertexPositionBuffer[p].itemSize, gl.FLOAT, false, 0, 0);

        setMatrixUniforms();
        gl.drawArrays(gl.TRIANGLES, 0, obraVertexPositionBuffer[p].numItems);

        mvPopMatrix();
   }

}





function initCuadros(){
    
    var i ;
    
    for ( i=0 ; i<mundo.obra.tama; i++ ){  
        
        
        var aux = gl.createTexture();
    
        aux.image = new Image();
        
        aux.image.src = mundo.obra.foto[i];
        
        if ( i==0 ){
            ima1 = aux ;
            
            ima1.image.onload = function (){
                handleLoadedTexture(ima1);

            };             
        }
        else if ( i==1 ){
            ima2 = aux ;
            
            ima2.image.onload = function (){
                handleLoadedTexture(ima2);

            };            
        }
        else if ( i==2 ){
            ima3 = aux ;
            
            ima3.image.onload = function (){
                handleLoadedTexture(ima3);

            };            
        }
        else if ( i==3 ){
            ima4 = aux ;
            
            ima4.image.onload = function (){
                handleLoadedTexture(ima4);

            };            
        }
        else if ( i==4 ){
            ima5 = aux ;
            
            ima5.image.onload = function (){
                handleLoadedTexture(ima5);

            };            
        }
        else if ( i==5 ){
            ima6 = aux ;
            
            ima6.image.onload = function (){
                handleLoadedTexture(ima6);

            };            
        }else if ( i==6 ){
            ima7 = aux ;
            
            ima7.image.onload = function (){
                handleLoadedTexture(ima7);

            };            
        }else if ( i==7 ){
            ima8 = aux ;
            
            ima8.image.onload = function (){
                handleLoadedTexture(ima8);

            };            
        }else if ( i==8 ){
            ima9 = aux ;
            
            ima9.image.onload = function (){
                handleLoadedTexture(ima9);

            };            
        }else if ( i==9 ){
            ima10 = aux ;
            
            ima10.image.onload = function (){
                handleLoadedTexture(ima10);

            };            
        }else if ( i==10 ){
            ima11 = aux ;
            
            ima11.image.onload = function (){
                handleLoadedTexture(ima11);

            };            
        }else if ( i==11 ){
            ima12 = aux ;
            
            ima12.image.onload = function (){
                handleLoadedTexture(ima12);

            };            
        }else if ( i==12 ){
            ima13 = aux ;
            
            ima13.image.onload = function (){
                handleLoadedTexture(ima13);

            };            
        }else if ( i==13 ){
            ima14 = aux ;
            
            ima14.image.onload = function (){
                handleLoadedTexture(ima14);

            };            
        }else if ( i==14 ){
            ima15 = aux ;
            
            ima15.image.onload = function (){
                handleLoadedTexture(ima15);

            };            
        }else if ( i==15 ){
            ima16 = aux ;
            
            ima16.image.onload = function (){
                handleLoadedTexture(ima16);

            };            
        }else if ( i==16 ){
            ima17 = aux ;
            
            ima17.image.onload = function (){
                handleLoadedTexture(ima17);

            };            
        }else if ( i==17 ){
            ima18 = aux ;
            
            ima18.image.onload = function (){
                handleLoadedTexture(ima18);

            };            
        }else if ( i==18 ){
            ima19 = aux ;
            
            ima19.image.onload = function (){
                handleLoadedTexture(ima19);

            };            
        }else if ( i==19 ){
            ima20 = aux ;
            
            ima20.image.onload = function (){
                handleLoadedTexture(ima20);

            };            
        }
     }
}