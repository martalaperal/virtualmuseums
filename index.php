<!DOCTYPE html>

<!--
    Document: Proyecto fin de carrera UAB-2010/2011
    Author: Marta Laperal Martin    
-->

<html lang="es">
    
<head>
    
    <?php
        session_start();
        header('Content-Type: text/html; charset=UTF-8');
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Proyecto Marta Laperal Martin 2010/2011</title>
    <link href="css/estilo.css" type="text/css" rel="stylesheet" media="screen">
    
    
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/rejilla.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/glMatrix-0.9.5.min.js"></script>
    <script type="text/javascript" src="js/webgl-utils.js"></script>
    <script type="text/javascript"  src="js/laberinto.js"></script> 
    <script id="shader-fs" type="x-shader/x-fragment">
    
        #ifdef GL_ES
        precision highp float;
        #endif
    
        varying vec2 vTextureCoord;
    
        uniform sampler2D uSampler;
    
        void main(void) {
            gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));
        }
    </script>
    
    <script id="shader-vs" type="x-shader/x-vertex">
    
        attribute vec3 aVertexPosition;
        attribute vec2 aTextureCoord;
    
        uniform mat4 uMVMatrix;
        uniform mat4 uPMatrix;
    
        varying vec2 vTextureCoord;
    
        void main(void) {
            gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
            vTextureCoord = aTextureCoord;
        }
    </script>
    
    
    
    
</head>

<body> 
    
    <div class="lateral" id="lateral1">
    
        <div class="container">        
                
            <div id="central">
                                
                <div id="cabecera">               
                    
                    <header>
                    <h1>Dise&ntilde;o de Museos Virtuales</h1>
                    </header>
                    
                    <div id="login">  
                        <?php
                            include('acceso/intro.php'); 
                        ?>                   
                    </div>
                </div>
            
            
                <div id="medio">                   
                    <nav>                                 
                        <div class="menu" id="menu2">                    
                            <ol class="seccion">
                                <li class="inicio">
                                    <a class="explicacion" id="p0" title="Volver al Inicio">Inicio</a>
                                </li>
                                <li class="logeo">
                                    <a class="explicacion" id="p1" title="Registrarse">Nuevo Usuario</a> 
                                </li>
                                <li class="autor">
                                    <a class="explicacion" id="p2" title="Acerca del autor">Acerca del autor</a>
                                </li>
                                <li class="contacto">
                                    <a class="explicacion" id="p3" title="Paleta de dibujo">Museo Virtual</a>
                                </li>
                                <li class="ayuda">
                                    <a class="explicacion" id="p4" title="Manual de usuario">Ayuda</a>
                                </li>
                            </ol>    
                        </div>
                    </nav>
                </div>
                
                <div id="cuerpo" name="cuerpo">
                    
                    <?php
                        if (!isset($_SESSION['exito']))
                            $_SESSION['exito']=0; // se inicializa
                        if (!isset($_SESSION['aviso']))
                            $_SESSION['aviso']=0; // se inicializa
                        if (!isset($_SESSION['guarda']))
                            $_SESSION['guarda']=0; // se inicializa
                        
                        $exito = $_SESSION['exito'] ;
                        $aviso = $_SESSION['aviso'] ;
                        $guarda = $_SESSION['guarda'] ;
                        
                        if ( $exito != 0 )
                            include('registro/datos.php');
                        else if ( $aviso != 0 || $guarda != 0 )
                            include('museo/virtual.php');
                        else
                            include('index/inicio.php');                   
                    ?>
                </div>
                
                <script type="text/javascript">            
                
                    $(document).ready(
                        function(){
                            $("#p0").click(
                                function(evento){
                                    evento.preventDefault();
                                    $("#cuerpo").load("index/inicio.php");
                                });
                        })
                    
                    $(document).ready(
                        function(){
                            $("#p1").click(
                                function(evento){
                                    evento.preventDefault();
                                    $("#cuerpo").load("registro/datos.php");
                                });
                        })
                    
                    $(document).ready(
                        function(){
                            $("#p2").click(
                                function(evento){
                                    evento.preventDefault();
                                    $("#cuerpo").load("autor/autora.php");
                                });
                        })
                    
                    $(document).ready(
                        function(){
                            $("#p3").click(
                                function(evento){
                                    evento.preventDefault();
                                    $("#cuerpo").load("museo/virtual.php");
                                });
                        })
                    
                    $(document).ready(
                        function(){
                            $("#p4").click(
                                function(evento){
                                    evento.preventDefault();
                                    $("#cuerpo").load("ayuda/manual.php");
                            });
                        })
                </script>
    
            
                <footer>
                    <div class="subfooter" id="up"></div>
                    <p><b>Realizado por:</b> Marta Laperal Mart&iacute;n</p>
                    <div class="subfooter" id="down"></div>
                </footer>
                
            </div>   
            
            <?php
            
                if($logeado==1){
                    echo "<script language='javascript'>;
                          alert('Error. El usuario o clave no existen o son incorrectos.'); 
                          </script>";
                }
            ?>        
        </div>
    
    </div>
</body>
</html>
