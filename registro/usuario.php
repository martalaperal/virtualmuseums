<?php

    if (!isset($_SESSION['exito']))
        $_SESSION['exito']=0; // se inicializa
        
    $exito=$_SESSION['exito'];        

    if ($exito==1){              
        echo "<script language='javascript'>";
        echo "alert('El registro se ha realizado satisfatoriamente.')";
        echo "</script>";
    }
    else if($exito==2){
        echo "<script language='javascript'>";
        echo "alert('Error. El numero de telefono es icorrecto.')";
        echo "</script>";
    }
    else if($exito==3){
        echo "<script language='javascript'>";
        echo "alert('Error. El correo introducido es incorrecto')";
        echo "</script>";
    }
    else if($exito==4){
        echo "<script language='javascript'>";
        echo "alert('Error. Ya existe un usuario con ese sobrenombre (nick).')";
        echo "</script>";
    }
    
    $_SESSION['exito']=0;
?>